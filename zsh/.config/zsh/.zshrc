export GPG_TTY="$(tty)"

autoload -Uz compinit && compinit
autoload -U colors && colors

PROMPT="%F{blue}%0~%f %(?..%F{red}%F{bold})%(?..%B%F{red})λ %f%b"

setopt SHARE_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt AUTO_CD

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

zstyle ':completion:*' zseparate-sections
zstyle ':completion:*' rehash true
zstyle ':completion:*' menu select
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$ZDOTDIR"/cache
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle -e ':completion:*:default' \
	   list-colors\
	   'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=00}:${LS_COLORS}")';

zstyle ":completion:*" list-colors "ma=48;5;14;1"

zmodload zsh/complist
_comp_options+=(globdots) # Include hidden files.
autoload predict-on

bindkey -M vicmd 'k' history-beginning-search-backward
bindkey -M vicmd 'j' history-beginning-search-forward
bindkey          "^[[A" history-beginning-search-backward
bindkey          "^[[B" history-beginning-search-forward

bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect '\x1b' send-break
bindkey -v '^?' backward-delete-char

#Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[2 q';;      # block
        viins|main) echo -ne '\e[6 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    echo -ne "\e[6 q"
}
zle -N zle-line-init
echo -ne '\e[6 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[6 q' ;} # Use beam shape cursor for each new prompt.

bindkey -s '^a' 'bc -l\n'

# declare a list of expandable aliases to fill up later
typeset -a ealiases
ealiases=()

# write a function for adding an alias to the list mentioned above
function abbr() {
    alias $1
    ealiases+=(${1%%\=*})
}

# expand any aliases in the current line buffer
function expand-ealias() {
    if [[ $LBUFFER =~ "\<(${(j:|:)ealiases})\$" ]]; then
        zle _expand_alias
        zle expand-word
    fi
    zle magic-space
}
zle -N expand-ealias

function expand-bang() {
    if [[ $LBUFFER =~ ".*!" ]]; then
		zle self-insert
		zle expand-word
	else
		zle self-insert
	fi
}
zle -N expand-bang

# A function for expanding any aliases before accepting the line as is and executing the entered command
expand-alias-and-accept-line() {
    expand-ealias
    zle .backward-delete-char

    zle .accept-line
}
zle -N accept-line expand-alias-and-accept-line


source "$HOME/.local/share/miniplug.zsh"

miniplug plugin 'zsh-users/zsh-autosuggestions'

miniplug load

# if [ "$TERM" = "linux" ]; then
# 	ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=7,bold'
# else
# 	ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
# fi
ZSH_AUTOSUGGEST_MANUAL_REBIND=true

ZSH_AUTOSUGGEST_STRATEGY=(history completion)

bindkey ' '            expand-ealias
bindkey '^ '           magic-space # control-space to bypass completion
bindkey -M isearch " " magic-space # normal space during searches
bindkey "!"            expand-bang # expand !!

abbr n='nvim'
abbr em='doas emerge'
abbr emu='doas emerge -uUD @world'
abbr emup="doas sh -c 'emerge -uUD @world && shutdown -P now'"
abbr d='doas'


alias \
	cp='cp -ri' \
	grep='grep --color' \
	ls='ls --color=auto' \
	la='ls -A' \
	ll='ls -lh' \
	lla='ll -A' \
	mkdir='mkdir -p' \
	mv='mv -i' \
	rm="rm -I" \
	ipinfo='curl ipinfo.io/ip' \
	irssi='irssi --config="$XDG_CONFIG_HOME"/irssi/config --home="$XDG_DATA_HOME"/irssi' \
	cpu="ps -Ao pid,comm,pmem,pcpu --sort=-pcpu | rmgr " \
	..='cd ..' \

upload0x0() {
	for arg in "$@"; do
		curl -F"file=@${arg}" https://0x0.st
	done
}
