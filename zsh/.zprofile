#!/bin/sh

# Default Programs
export BROWSER="vimb_launch"

export VISUAL="nvim"
export EDITOR="$VISUAL"
export GIT_EDITOR="$EDITOR"

export MANPAGER='less -s -M +Gg'
export READER="zathura"
export TERMINAL="st"

# XDG direcroties
export XDG_RUNTIME_DIR=/tmp/runtime-"$USER"
if ! test -d "${XDG_RUNTIME_DIR}"; then
	mkdir "${XDG_RUNTIME_DIR}"
	chmod 0700 "${XDG_RUNTIME_DIR}"
fi
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_DATA_HOME="$HOME"/.local/share

# ~/ Clean up
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GOPATH="$XDG_DATA_HOME"/go
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc-2.0
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export LESSHISTFILE="-"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME"/notmuch-config
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export PASSWORD_STORE_DIR="$XDG_CONFIG_HOME"/password-store
export PLTUSERHOME="$XDG_DATA_HOME"/racket
export PYLINTHOME="$XDG_CACHE_HOME"/pylint
export PYTHONSTARTUP="$XDG_CONFIG_HOME"/python/pythonstartup
export TEXFCONFIG="$XDG_CONFIG_HOME"/texlive/texmf-config
export TEXFVAR="$XDG_CONFIG_HOME"/texlive/texmf-var
export TASKRC="$XDG_CONFIG_HOME"/task/taskrc
export XMONAD_CACHE_HOME="$XDG_CACHE_DIR"/xmonad
export XMONAD_CONFIG_HOME="$XDG_CONFIG_DIR"/xmonad
export XMONAD_DATA_HOME="$XDG_DATA_DIR"/xmonad
export ZDOTDIR="$XDG_CONFIG_HOME"/zsh
#export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export STACK_ROOT="$XDG_DATA_HOME"/stack

# Programs configurations
export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT="MToolkit wmname LG3D"

# Less colors
export LESS_TERMCAP_mb=$(printf "\e[1;34m")
export LESS_TERMCAP_md=$(printf "\e[1;34m")
export LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so=$(printf '\e[01;04;07;33m')
export LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us=$(printf "\e[4;36m")

# ls colors
eval "$(dircolors)"

export PATH="$PATH:$HOME/.local/bin:$HOME/.local/bin/barscripts:$GOPATH/bin"

# Starts xorg on login
[ "$(tty)" = "/dev/tty1" ] && ! pidof X > /dev/null && exec sx
