-- Tabs: {{{

local settings = require "settings"
local lousy = require "lousy"
local modes = require "modes"

-- Tab number
lousy.widget.tab.label_format = '<span>{index}: </span>{title}'

-- Add ordering of new tabs
local taborder = require "taborder"

taborder.default = function(w)
	local order = taborder.after_current (w)
	return order
end
-- }}}
-- Binds {{{

-- Bind control-c to copy
modes.add_binds("normal", {{
    "<Control-c>",
    "Copy selected text.",
    function ()
        luakit.selection.clipboard = luakit.selection.primary
    end
}})

-- Open on umpv with ;j or g;j
modes.add_binds("ex-follow", {{ 
	"n", 
  	"open in umpv",
  	function (w)
    	w:set_mode("follow", {
      	prompt = "video", selector = "uri", evaluator = "uri",
      	func = function (uri)
			assert(type(uri) == "string")
			uri = string.gsub(uri, " ", "%%20")
			luakit.selection.primary = uri
			luakit.spawn(string.format("umpv '%s'", uri))
      end
    })
  end },
})

modes.add_binds("normal", {
	{"qm", "List all quickmarks.", function (w) w:set_mode("qmarklist") end}
}) 

modes.add_binds("normal", {
    { "cn", "Play video in page", function (w)
        local view = w.view
        local uri = view.uri
        if uri then
			luakit.spawn(string.format("umpv '%s'", uri))
        end
    end },
})

modes.add_binds("normal", {
    { "ba", "Add bookmark to bookmarksman", function (w)
        local view = w.view
        local uri = view.uri
        if uri then
			luakit.spawn(string.format("bookmarksman '%s'", uri))
        end
    end },
})

local select = require "select"
local follow = require "follow"

select.label_maker = function ()
    local chars = charset("arstneio")
    return trim(sort(reverse(chars)))
end

follow.pattern_maker = follow.pattern_styles.match_label
follow.ignore_delay = 0
-- }}}
-- General configs {{{

-- Sets download dir
local downloads = require "downloads"

downloads.default_dir = os.getenv("HOME") .. "/dlw"

-- Sets search engines
local engines = settings.window.search_engines
settings.window.new_tab_page = "file:///home/cassio/doc/startpage/index.html"


-- w.bar_layout.visible = not w.bar_layout.visible
modes.add_binds("normal", {
    {
        "<Control-n>", "Toggle statusbar", function(w)
    		w.bar_layout.visible = not w.bar_layout.visible
        end},
    })

engines.default = "https://duckduckgo.com/?q=%s"
engines.aw      = "https://wiki.archlinux.org/index.php?search=%s"
engines.gp      = "https://packages.gentoo.org/packages/search?q=%s"
engines.gw      = "https://wiki.gentoo.org/?search=%s"
engines.go      = "https://gentoo.zugaina.org/Search?search=%s"

-- }}}
-- Signals: {{{

-- Sets up notifications and link handling
local webview = require "webview"

webview.add_signal("init", function (view)
    view:add_signal("permission-request", function (v, type)
        if type == "notification" then return true end -- Might want to check v.uri too
    end)
end)

webview.add_signal("init", function (view)
    view:add_signal("navigation-request", function (v, uri)
        if string.match(string.lower(uri), "^magnet:") then
			luakit.spawn(string.format("xdg-open \"%s\"", uri))
			return false
		elseif string.match(string.lower(uri), "meet.google") then
			luakit.spawn(string.format("firefox-bin \"%s\"", uri))
			return false
    	end
    end)
end)

-- Change spell check language based on uri
luakit.enable_spell_checking = true

local window = require "window"


local rescan = function ()
    for _, w in pairs(window.bywidget) do
		if w.view.uri then
			if w.view.uri:find("web.whatsapp.com") then
				luakit.spell_checking_languages = { "pt_BR" }
			else
				luakit.spell_checking_languages = { "en_US" }
			end
		end
    end
end

webview.add_signal("init", function (view)
    view:add_signal("property::uri", rescan)
    view:add_signal("switched-page", rescan)
end)
-- vim: fdm=marker
-- }}}
