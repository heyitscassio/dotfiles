#!/bin/sh

if ! $(getumpvpause.py) && ! $(cmus-remote -Q 2> /dev/null | grep -q playing); then
	pidof aplay > /dev/null && kill -KILL "$(pidof aplay)"
	aplay -D default -c 2 -s 3500 -q -f S16_LE ~/.local/share/sounds/notification2.wav
fi
