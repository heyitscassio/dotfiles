require 'mp'


local function appendDeletelist()
	path = mp.get_property_native("path")

	filelist = io.open(".deletelist", "a")
	io.output(filelist)
	io.write(path .. "\n")
	io.close(filelist)

	mp.osd_message("added " .. path .. " to deletelist")
end

mp.add_key_binding("x", "deletelist", appendDeletelist)
